// <auto-generated />
namespace Genealogist.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class unknownDates : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(unknownDates));
        
        string IMigrationMetadata.Id
        {
            get { return "201511202115243_unknownDates"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
