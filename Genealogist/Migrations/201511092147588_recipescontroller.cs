namespace Genealogist.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class recipescontroller : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.recipes",
                c => new
                    {
                        recipeID = c.Int(nullable: false, identity: true),
                        ShortName = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.recipeID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.recipes");
        }
    }
}
