namespace Genealogist.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class retirement : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.cakes", "Retired", c => c.Boolean(nullable: false));
            AddColumn("dbo.syringes", "Retired", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.syringes", "Retired");
            DropColumn("dbo.cakes", "Retired");
        }
    }
}
