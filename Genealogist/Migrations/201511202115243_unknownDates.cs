namespace Genealogist.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class unknownDates : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.syringes", "CreationDate", c => c.DateTime());
            AlterColumn("dbo.prints", "CreationDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.prints", "CreationDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.syringes", "CreationDate", c => c.DateTime(nullable: false));
        }
    }
}
