namespace Genealogist.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class othercontrollers : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.cakes",
                c => new
                    {
                        cakeID = c.Int(nullable: false, identity: true),
                        JarID = c.Int(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.cakeID)
                .ForeignKey("dbo.jars", t => t.JarID, cascadeDelete: true)
                .Index(t => t.JarID);
            
            CreateTable(
                "dbo.jars",
                c => new
                    {
                        jarID = c.Int(nullable: false, identity: true),
                        JarNumber = c.Int(nullable: false),
                        CreationDate = c.DateTime(nullable: false),
                        RecipeID = c.Int(nullable: false),
                        SterilizationDate = c.DateTime(),
                        InnoculationDate = c.DateTime(),
                        SyringeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.jarID)
                .ForeignKey("dbo.recipes", t => t.RecipeID, cascadeDelete: true)
                .ForeignKey("dbo.syringes", t => t.SyringeID, cascadeDelete: true)
                .Index(t => t.RecipeID)
                .Index(t => t.SyringeID);
            
            CreateTable(
                "dbo.syringes",
                c => new
                    {
                        syringeID = c.Int(nullable: false, identity: true),
                        CreationDate = c.DateTime(nullable: false),
                        PrintID = c.Int(),
                    })
                .PrimaryKey(t => t.syringeID)
                .ForeignKey("dbo.prints", t => t.PrintID)
                .Index(t => t.PrintID);
            
            CreateTable(
                "dbo.prints",
                c => new
                    {
                        printID = c.Int(nullable: false, identity: true),
                        FlushID = c.Int(),
                        CreationDate = c.DateTime(nullable: false),
                        PCT = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.printID)
                .ForeignKey("dbo.flushes", t => t.FlushID)
                .Index(t => t.FlushID);
            
            CreateTable(
                "dbo.flushes",
                c => new
                    {
                        flushID = c.Int(nullable: false, identity: true),
                        CakeID = c.Int(nullable: false),
                        HarvestDate = c.DateTime(),
                        DessicationEndDate = c.DateTime(),
                        DryWeight = c.Double(),
                        DessicationChamber = c.Int(),
                        comments = c.String(),
                    })
                .PrimaryKey(t => t.flushID)
                .ForeignKey("dbo.cakes", t => t.CakeID, cascadeDelete: true)
                .Index(t => t.CakeID);
            
            CreateTable(
                "dbo.dunks",
                c => new
                    {
                        dunkID = c.Int(nullable: false, identity: true),
                        CakeID = c.Int(nullable: false),
                        DateStarted = c.DateTime(nullable: false),
                        DateEnded = c.DateTime(),
                    })
                .PrimaryKey(t => t.dunkID)
                .ForeignKey("dbo.cakes", t => t.CakeID, cascadeDelete: true)
                .Index(t => t.CakeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.dunks", "CakeID", "dbo.cakes");
            DropForeignKey("dbo.cakes", "JarID", "dbo.jars");
            DropForeignKey("dbo.jars", "SyringeID", "dbo.syringes");
            DropForeignKey("dbo.syringes", "PrintID", "dbo.prints");
            DropForeignKey("dbo.prints", "FlushID", "dbo.flushes");
            DropForeignKey("dbo.flushes", "CakeID", "dbo.cakes");
            DropForeignKey("dbo.jars", "RecipeID", "dbo.recipes");
            DropIndex("dbo.dunks", new[] { "CakeID" });
            DropIndex("dbo.flushes", new[] { "CakeID" });
            DropIndex("dbo.prints", new[] { "FlushID" });
            DropIndex("dbo.syringes", new[] { "PrintID" });
            DropIndex("dbo.jars", new[] { "SyringeID" });
            DropIndex("dbo.jars", new[] { "RecipeID" });
            DropIndex("dbo.cakes", new[] { "JarID" });
            DropTable("dbo.dunks");
            DropTable("dbo.flushes");
            DropTable("dbo.prints");
            DropTable("dbo.syringes");
            DropTable("dbo.jars");
            DropTable("dbo.cakes");
        }
    }
}
