﻿using Genealogist.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Genealogist.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            ViewBag.ActiveSyringes = db.syringes.Where(s => s.Retired == false);
            ViewBag.ActivePrints = db.prints.Where(p => true); //TODO: add a "retired" or "used" value or something
            ViewBag.ActiveJars = db.jars.Where(j => db.cakes.LongCount(c => c.JarID == j.jarID) == 0);
            ViewBag.ActiveDunks = db.dunks.Where(d => d.DateStarted != null && d.DateEnded == null);
            ViewBag.FruitingCakes = db.cakes.Where(c => c.Retired == false && db.dunks.LongCount(d => d.CakeID == c.cakeID && d.DateEnded != null) == 0);
            ViewBag.DryingFlushes = db.flushes.Where(f => f.DessicationEndDate == null);
            return View();
        }
    }
}
