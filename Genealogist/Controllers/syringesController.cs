﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Genealogist.Models;

namespace Genealogist
{
    public class syringesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: syringes
        public ActionResult Index()
        {
            var syringes = db.syringes.Include(s => s.Print);
            return View(syringes.ToList());
        }

        // GET: syringes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            syringe syringe = db.syringes.Find(id);
            if (syringe == null)
            {
                return HttpNotFound();
            }
            return View(syringe);
        }

        // GET: syringes/Create
        public ActionResult Create()
        {
            ViewBag.SyringeID = db.syringes.Max(model => model.syringeID) + 1;
            ViewBag.PrintID = new SelectList(db.prints, "printID", "printID");
            return View();
        }

        // POST: syringes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "syringeID,CreationDate,PrintID")] syringe syringe)
        {
            if (ModelState.IsValid)
            {
                db.syringes.Add(syringe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.SyringeID = syringe.syringeID;
            ViewBag.PrintID = new SelectList(db.prints, "printID", "printID");
            return View();
        }

        // GET: syringes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            syringe syringe = db.syringes.Find(id);
            if (syringe == null)
            {
                return HttpNotFound();
            }
            ViewBag.SyringeID = id;
            ViewBag.PrintID = new SelectList(db.prints, "printID", "printID", syringe.PrintID);
            return View(syringe);
        }

        // POST: syringes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "syringeID,CreationDate,PrintID")] syringe syringe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(syringe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PrintID = new SelectList(db.prints, "printID", "printID", syringe.PrintID);
            return View(syringe);
        }

        // GET: syringes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            syringe syringe = db.syringes.Find(id);
            if (syringe == null)
            {
                return HttpNotFound();
            }
            return View(syringe);
        }

        // POST: syringes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            syringe syringe = db.syringes.Find(id);
            db.syringes.Remove(syringe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
