﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Genealogist.Models;

namespace Genealogist
{
    public class cakesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: cakes
        public ActionResult Index()
        {
            var cakes = db.cakes.Include(c => c.Jar);
            return View(cakes.ToList());
        }

        // GET: cakes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cake cake = db.cakes.Find(id);
            if (cake == null)
            {
                return HttpNotFound();
            }
            ViewBag.dunkHistory = db.dunks.Where(d => d.CakeID == cake.cakeID);
            return View(cake);
        }

        // GET: cakes/Create
        public ActionResult Create()
        {
            ViewBag.JarID = new SelectList(db.jars, "jarID", "jarID");
            return View();
        }

        // POST: cakes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "cakeID,JarID,BirthDate")] cake cake)
        {
            if (ModelState.IsValid)
            {
                db.cakes.Add(cake);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.JarID = new SelectList(db.jars, "jarID", "jarID", cake.JarID);
            return View(cake);
        }

        // GET: cakes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cake cake = db.cakes.Find(id);
            if (cake == null)
            {
                return HttpNotFound();
            }
            ViewBag.JarID = new SelectList(db.jars, "jarID", "jarID", cake.JarID);
            return View(cake);
        }

        // POST: cakes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "cakeID,JarID,BirthDate")] cake cake)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cake).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.JarID = new SelectList(db.jars, "jarID", "jarID", cake.JarID);
            return View(cake);
        }

        // GET: cakes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cake cake = db.cakes.Find(id);
            if (cake == null)
            {
                return HttpNotFound();
            }
            return View(cake);
        }

        // POST: cakes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            cake cake = db.cakes.Find(id);
            db.cakes.Remove(cake);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
