﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Genealogist.Models;

namespace Genealogist
{
    public class flushesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: flushes
        public ActionResult Index()
        {
            var flushes = db.flushes.Include(f => f.Cake);
            return View(flushes.ToList());
        }

        // GET: flushes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            flush flush = db.flushes.Find(id);
            if (flush == null)
            {
                return HttpNotFound();
            }
            return View(flush);
        }

        // GET: flushes/Create
        public ActionResult Create()
        {
            ViewBag.CakeID = new SelectList(db.cakes, "cakeID", "cakeID");
            return View();
        }

        // POST: flushes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "flushID,CakeID,HarvestDate,DessicationEndDate,DryWeight,DessicationChamber,comments")] flush flush)
        {
            if (ModelState.IsValid)
            {
                db.flushes.Add(flush);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CakeID = new SelectList(db.cakes, "cakeID", "cakeID", flush.CakeID);
            return View(flush);
        }

        // GET: flushes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            flush flush = db.flushes.Find(id);
            if (flush == null)
            {
                return HttpNotFound();
            }
            ViewBag.CakeID = new SelectList(db.cakes, "cakeID", "cakeID", flush.CakeID);
            return View(flush);
        }

        // POST: flushes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "flushID,CakeID,HarvestDate,DessicationEndDate,DryWeight,DessicationChamber,comments")] flush flush)
        {
            if (ModelState.IsValid)
            {
                db.Entry(flush).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CakeID = new SelectList(db.cakes, "cakeID", "cakeID", flush.CakeID);
            return View(flush);
        }

        // GET: flushes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            flush flush = db.flushes.Find(id);
            if (flush == null)
            {
                return HttpNotFound();
            }
            return View(flush);
        }

        // POST: flushes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            flush flush = db.flushes.Find(id);
            db.flushes.Remove(flush);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
