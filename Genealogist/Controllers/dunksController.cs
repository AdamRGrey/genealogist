﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Genealogist.Models;

namespace Genealogist.Controllers
{
    public class dunksController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: dunks
        public ActionResult Index()
        {
            var dunks = db.dunks.Include(d => d.Cake);
            return View(dunks.ToList());
        }

        // GET: dunks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            dunk dunk = db.dunks.Find(id);
            if (dunk == null)
            {
                return HttpNotFound();
            }
            return View(dunk);
        }

        // GET: dunks/Create
        public ActionResult Create()
        {
            ViewBag.CakeID = new SelectList(db.cakes, "cakeID", "cakeID");
            return View();
        }

        // POST: dunks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "dunkID,CakeID,DateStarted,DateEnded")] dunk dunk)
        {
            if (ModelState.IsValid)
            {
                db.dunks.Add(dunk);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CakeID = new SelectList(db.cakes, "cakeID", "cakeID", dunk.CakeID);
            return View(dunk);
        }

        // GET: dunks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            dunk dunk = db.dunks.Find(id);
            if (dunk == null)
            {
                return HttpNotFound();
            }
            ViewBag.CakeID = new SelectList(db.cakes, "cakeID", "cakeID", dunk.CakeID);
            return View(dunk);
        }

        // POST: dunks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "dunkID,CakeID,DateStarted,DateEnded")] dunk dunk)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dunk).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CakeID = new SelectList(db.cakes, "cakeID", "cakeID", dunk.CakeID);
            return View(dunk);
        }

        // GET: dunks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            dunk dunk = db.dunks.Find(id);
            if (dunk == null)
            {
                return HttpNotFound();
            }
            return View(dunk);
        }

        // POST: dunks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            dunk dunk = db.dunks.Find(id);
            db.dunks.Remove(dunk);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
