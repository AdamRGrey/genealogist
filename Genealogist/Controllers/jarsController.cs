﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Genealogist.Models;

namespace Genealogist
{
    public class jarsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: jars
        public ActionResult Index()
        {
            var jars = db.jars.Include(j => j.Recipe).Include(j => j.Syringe);
            return View(jars.ToList());
        }

        // GET: jars/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            jar jar = db.jars.Find(id);
            if (jar == null)
            {
                return HttpNotFound();
            }
            return View(jar);
        }

        // GET: jars/Create
        public ActionResult Create()
        {
            ViewBag.RecipeID = new SelectList(db.recipes, "recipeID", "ShortName");
            ViewBag.SyringeID = new SelectList(db.syringes, "syringeID", "syringeID");
            return View();
        }

        // POST: jars/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "jarID,JarNumber,CreationDate,RecipeID,SterilizationDate,InnoculationDate,SyringeID")] jar jar)
        {
            if (ModelState.IsValid)
            {
                db.jars.Add(jar);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RecipeID = new SelectList(db.recipes, "recipeID", "ShortName", jar.RecipeID);
            ViewBag.SyringeID = new SelectList(db.syringes, "syringeID", "syringeID", jar.SyringeID);
            return View(jar);
        }

        // GET: jars/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            jar jar = db.jars.Find(id);
            if (jar == null)
            {
                return HttpNotFound();
            }
            ViewBag.RecipeID = new SelectList(db.recipes, "recipeID", "ShortName", jar.RecipeID);
            ViewBag.SyringeID = new SelectList(db.syringes, "syringeID", "syringeID", jar.SyringeID);
            return View(jar);
        }

        // POST: jars/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "jarID,JarNumber,CreationDate,RecipeID,SterilizationDate,InnoculationDate,SyringeID")] jar jar)
        {
            if (ModelState.IsValid)
            {
                db.Entry(jar).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.RecipeID = new SelectList(db.recipes, "recipeID", "ShortName", jar.RecipeID);
            ViewBag.SyringeID = new SelectList(db.syringes, "syringeID", "syringeID", jar.SyringeID);
            return View(jar);
        }

        // GET: jars/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            jar jar = db.jars.Find(id);
            if (jar == null)
            {
                return HttpNotFound();
            }
            return View(jar);
        }

        // POST: jars/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            jar jar = db.jars.Find(id);
            db.jars.Remove(jar);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
