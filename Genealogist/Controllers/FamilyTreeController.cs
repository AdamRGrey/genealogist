﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Genealogist.Models;

namespace Genealogist.Controllers
{
    public class FamilyTreeController : Controller
    {
        public const uint SMALL_PARENT_GENS = 2;
        public const uint SMALL_CHILD_GENS = 2;
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Small(int id, string type)
        {
            FamilyTree tree = new FamilyTree();
            object found = null;
            if (type == (typeof(jar)).ToString())
            {
                found = db.jars.Find(id);
            }
            else if (type == typeof(syringe).ToString())
            {
                found = db.syringes.Find(id);
            }
            else if (type == typeof(cake).ToString())
            {
                found = db.cakes.Find(id);
            }
            else if (type == typeof(print).ToString())
            {
                found = db.prints.Find(id);
            }
            else if (type == typeof(flush).ToString())
            {
                found = db.flushes.Find(id);
            }
            tree.Build(SMALL_PARENT_GENS, SMALL_CHILD_GENS, found);
            return View(tree);
        }
    }
}
