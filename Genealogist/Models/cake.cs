﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Genealogist.Models
{
    public class cake
    {
        public int cakeID { get; set; }
        public int JarID { get; set; }
        /// <summary>
        /// jar I was born from
        /// </summary>
        public jar Jar { get; set; }
        public DateTime BirthDate { get; set; }
        [DefaultValue(false)]
        public bool Retired { get; set; }
    }
}