﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Genealogist.Models
{
    public enum PrintCompositionType
    {
        ManySmall, OneLarge
    }
    public class print
    {
        public int printID { get; set; }
        public int? FlushID { get; set; }
        public flush Flush { get; set; }
        public DateTime? CreationDate { get; set; }
        public PrintCompositionType PCT { get; set; }
    }
}