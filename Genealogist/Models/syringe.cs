﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Genealogist.Models
{
    public class syringe
    {
        /// <summary>
        /// the unique ID of each syringe. will have to write this on each one.
        /// </summary>
        public int syringeID { get; set; }
        public DateTime? CreationDate { get; set; }
        [DisplayName("Parent print")]
        public int? PrintID { get; set; }
        public print Print { get; set; }
        [DefaultValue(false)]
        public bool Retired { get; set; }
    }
}