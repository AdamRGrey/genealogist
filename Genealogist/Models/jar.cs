﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Genealogist.Models
{
    public class jar
    {
        /// <summary>
        /// unique to each instance, so If I re-use a physical jar, I'll get different IDs.
        /// </summary>
        public int jarID { get; set; }
        /// <summary>
        /// the number written on the jar. This will be low, and is not unique.
        /// </summary>
        public int JarNumber { get; set; }
        public DateTime CreationDate { get; set; }
        public int RecipeID { get; set; }
        public recipe Recipe { get; set; }
        public DateTime? SterilizationDate { get; set; }
        public DateTime? InnoculationDate { get; set; }
        public int SyringeID { get; set; }
        public syringe Syringe { get; set; }
    }
}