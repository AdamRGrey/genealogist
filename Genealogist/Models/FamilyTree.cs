﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace Genealogist.Models
{
    public class FamilyTreeNode
    {
        public object data;
        public List<FamilyTreeNode> children = new List<FamilyTreeNode>();
    }
    public class FamilyTree
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public FamilyTreeNode focusNode = new FamilyTreeNode();
        public FamilyTreeNode rootNode;
        public void Build(uint maxParentGenerations, uint maxChildGenerations, object o)
        {
            focusNode.data = o;
            if(maxParentGenerations > 0)
                rootNode = getParentNode(focusNode, maxParentGenerations);
            else
                rootNode = focusNode;
            getDescendants(rootNode, maxChildGenerations + maxParentGenerations);
        }

        public FamilyTreeNode getParentNode(FamilyTreeNode f, uint parentGenerations)
        {
            if(parentGenerations == 0)
                return f;
            var parent = new FamilyTreeNode();
            f.children = new List<FamilyTreeNode>();
            if(f.data is jar)
            {
                parent.data = (f.data as jar).Syringe;
            }
            if(f.data is syringe)
            {
                parent.data = (f.data as syringe).Print;
            }
            if(f.data is print)
            {
                parent.data = (f.data as print).Flush;
            }
            if(f.data is flush)
            {
                parent.data = (f.data as flush).Cake;
            }
            if(f.data is cake)
            {
                parent.data = (f.data as cake).Jar;
            }
            if(parent.data == null)
            {
                parent = null;
                return f;
            }
            parent.children.Add(f);
            return getParentNode(parent, parentGenerations-1);
        }
        public void getDescendants(FamilyTreeNode node, uint childGenerations)
        {
            if (childGenerations == 0)
                return;
            FamilyTreeNode child = null;
            if(node.children.Count > 0)
                child = node.children[0];
            IEnumerable myCollection = null;
            //There must be a better way to do this.
            if (node.data is jar)
            {
                jar j = node.data as jar;
                myCollection = db.cakes.Where((c) => c.JarID == j.jarID);
            }
            if (node.data is syringe)
            {
                syringe s = node.data as syringe;
                myCollection = db.jars.Where((j) => j.SyringeID == s.syringeID);
            }
            if (node.data is print)
            {
                print p = node.data as print;
                myCollection = db.syringes.Where((s) => s.PrintID == p.printID);
            }
            if (node.data is flush)
            {
                flush f = node.data as flush;
                myCollection = db.prints.Where((p) => p.FlushID == f.flushID);
            }
            if (node.data is cake)
            {
                cake c = node.data as cake;
                myCollection = db.flushes.Where((f) => f.CakeID == c.cakeID);
            }
            foreach (var item in myCollection)
            {
                if (child == null || child.data != item)
                {
                    FamilyTreeNode newNode = new FamilyTreeNode();
                    newNode.data = item;
                    node.children.Add(newNode);
                }
            }
            foreach (var item in node.children)
            {
                getDescendants(item, childGenerations - 1);
            }
        }
        public override string ToString()
        {
            var rootNodeJSON = new JavaScriptSerializer().Serialize(rootNode);
            return rootNodeJSON;
        }
    }
}