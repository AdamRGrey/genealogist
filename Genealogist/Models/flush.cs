﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Genealogist.Models
{
    public class flush
    {
        public int flushID { get; set; }
        public int CakeID { get; set; }
        /// <summary>
        /// cake I came from
        /// </summary>
        public cake Cake { get; set; }
        /// <summary>
        /// the day we pulled the flush off the cake. Also the moment we started dessicating;
        /// they don't sit around wet.
        /// </summary>
        public DateTime? HarvestDate { get; set; }
        /// <summary>
        /// True Harvest Day. The day they're dry and moved into a bag.
        /// </summary>
        public DateTime? DessicationEndDate { get; set; }
        public double? DryWeight { get; set; }
        public int? DessicationChamber { get; set; }
        public string comments { get; set; }
    }
}